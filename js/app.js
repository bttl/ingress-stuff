ractive = new Ractive({
  el: 'badgeDisplay',
  template: '#badgeTemplate',
  data: {
    user: "G0THAR",
    userTail: "G0THAR_______",
    faction: "RESISTANCE",
    level: 4,
    cell: "AM03-GOLF-06",
    community: "DFW Community OPS",
    link: "dfwresistance.us",
    levelbars: [
      "#0060ff", "#0060ff", "#0060ff", "#0060ff", "FFF", "FFF", "FFF", "FFF", "FFF", "FFF", "FFF", "FFF", "FFF", "FFF", "FFF", "FFF"
    ],
    leveltxt: '#000000',
    factioncolor: '#0060ff',
    resistDisplay: 'block',
    enlightDisplay: 'none',
    fimageDisplay: 'none',
    factionTranslate: 'translate(0,0)',
    factionScale: 'scale(0.92656366,1.0792567)',
    qr_image: 'images/dfwresistance-qr.png',
    f_image: ''
  }
});

document.getElementById('error').style.display = 'none';
document.getElementById('size').style.display = 'none';
rotated = false;

function removeClass(el, className) {
  if (el.classList) {
    el.classList.remove(className);
  } else {
    el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  }
}

function highlight(id) {
  removeClass(document.getElementById('helps1'), 'active');
  removeClass(document.getElementById('helps2'), 'active');
  removeClass(document.getElementById('helps3'), 'active');
  removeClass(document.getElementById('helps4'), 'active');
  document.getElementById(id).className += ' ' + 'active';
}

function fileFilter(file) {
  var Filter = /^(image\/png)$/i;
  if (! Filter.test(file.type) ) {
    return false;
  } else {
    return true;
  }
}

function sizeFilter(file) {
  if (file.size > 1048576) {
    return false;
  } else {
    return true;
  }
}

function qrSelected() {
  document.getElementById('error').style.display = 'none';
  document.getElementById('size').style.display = 'none';

  var qrFile = document.getElementById('qr_code').files[0];

  if (! fileFilter(qrFile)) {
    document.getElementById('error').style.display = '';
    return
  }

  if (! sizeFilter(qrFile)) {
    document.getElementById('size').style.display = '';
    return;
  }

  var qrReader = new FileReader();
  qrReader.onload = function(e) {
    ractive.set({qr_image: e.target.result});
  }
  qrReader.readAsDataURL(qrFile);
}

function fSelected() {
  document.getElementById('error').style.display = 'none';
  document.getElementById('size').style.display = 'none';

  var fFile = document.getElementById('f_image').files[0];

  if (! fileFilter(fFile)) {
    document.getElementById('error').style.display = '';
    return
  }

  if (! sizeFilter(fFile)) {
    document.getElementById('size').style.display = '';
    return;
  }

  var fReader = new FileReader();
  fReader.onload = function(e) {
    ractive.set({f_image: e.target.result, resistDisplay: 'none', enlightDisplay: 'none', fimageDisplay: 'block'});

  }
  fReader.readAsDataURL(fFile);

}


document.addEventListener('DOMContentLoaded', function(all){
  function levelColor(level) {
    var fcolor = ractive.get('factioncolor');
    var colordata = {
      0: ['#FFA500', ['#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      1: ['#000000', [fcolor, '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      2: ['#000000', [fcolor, fcolor, '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      3: ['#000000', [fcolor, fcolor, fcolor, '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      4: ['#000000', [fcolor, fcolor, fcolor, fcolor, '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      5: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      6: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      7: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      8: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      9: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#febf5f', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      10: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#febf5f', '#febf5f', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      11: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#febf5f', '#febf5f', '#febf5f', '#FFF', '#FFF', '#FFF', '#FFF', '#FFF']],
      12: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#FFF', '#FFF', '#FFF', '#FFF']],
      13: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#FFF', '#FFF', '#FFF']],
      14: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#FFF', '#FFF']],
      15: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#FFF']],
      16: ['#000000', [fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, fcolor, '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f', '#febf5f']]
    }
    if (level < 0 || level > 16) {
      ractive.animate({ leveltxt: "#FFF", levelbars: colordata['0'][1] }, {
        easing: 'easeOut',
        duration: 600
      });
    } else {
      ractive.animate({ leveltxt: colordata[level][0], levelbars: colordata[level][1] }, {
        easing: 'easeOut',
        duration: 600
      });
    }
  }
  levelObserver = ractive.observe( 'level', function ( newValue, oldValue, keypath ) {
    levelColor(newValue);
    document.getElementById('savebuttons').setAttribute('style', 'display: none;');
  });
  userObserver = ractive.observe( 'user', function ( newValue, oldValue, keypath ) {
    document.getElementById('savebuttons').setAttribute('style', 'display: none;');
    var length = newValue.length;
    if (length >= 13) {
      newValue = newValue.slice(0,13);
      ractive.set({userTail: newValue});
    } else {
      var tail = "_____________";
      newValue = newValue + tail.slice(0, 13 - length);
      ractive.set({userTail: newValue});
    }
  });
  factionObserver = ractive.observe( 'faction', function ( newValue, oldValue, keypath ) {
    document.getElementById('savebuttons').setAttribute('style', 'display: none;');
    if (newValue == 'RESISTANCE') {
      ractive.animate({ factioncolor: '#0060ff', resistDisplay: 'block', enlightDisplay: 'none', fimageDisplay: 'none', factionTranslate: 'translate(0,0)', factionScale: 'scale(0.92656366,1.0792567)'}, {
        easing: 'easeOut',
        duration: 800
      });
      levelColor(ractive.get('level'));
    } else {
      ractive.animate({ factioncolor: '#8DC641', resistDisplay: 'none', enlightDisplay: 'block', fimageDisplay: 'none', factionTranslate: 'translate(6,0)', factionScale: 'scale(0.85656366,1.0792567)'}, {
        easing: 'easeOut',
        duration: 800
      });
      levelColor(ractive.get('level'));
    }
  });
  restObserver = ractive.observe({'cell': function(nO, oO) {
      document.getElementById('savebuttons').setAttribute('style', 'display: none;');
    },
    'link': function(nO, oO) {
      document.getElementById('savebuttons').setAttribute('style', 'display: none;');
    },
    'community': function(nO, oO) {
      document.getElementById('savebuttons').setAttribute('style', 'display: none;');
    },
    'qr_image': function(nO, oO) {
      document.getElementById('savebuttons').setAttribute('style', 'display: none;');
    },
    'f_image': function(nO, oO) {
      document.getElementById('savebuttons').setAttribute('style', 'display: none;');
    }
  });
  document.getElementById('level-up').addEventListener( 'click', function (e) {
    var levelup = (ractive.get('level') + 1) % 17;
    ractive.set('level', levelup);
    levelColor(levelup);
    e.preventDefault();
  });
  document.getElementById('level-down').addEventListener( 'click', function (e) {
    var oldlevel = ractive.get('level');
    if (oldlevel <= 0) {
      oldlevel = 1;
    }
    var leveldown = (oldlevel - 1);
    ractive.set('level', leveldown);
    levelColor(leveldown);
    e.preventDefault();
  });
  document.getElementById('rotate').addEventListener( 'click', function (e) {
    if (!rotated) {
      document.getElementById('front').className += ' ' + 'front-rotate';
      document.getElementById('back').className += ' ' + 'back-rotate';
      rotated = true;
    } else {
      removeClass(document.getElementById('front'), 'front-rotate');
      removeClass(document.getElementById('back'), 'back-rotate');
      rotated = false;
    }
    e.preventDefault();
  });
  document.getElementById('saveimage').addEventListener( 'click', function (e) {
    e.preventDefault();
    var frontcan = document.getElementById('frontimage');
    canvg(frontcan, document.getElementById('front').innerHTML, {log: true, ignoreAnimation: true});
    frontcan.click();
    var backcan = document.getElementById('backimage');
    canvg(backcan, document.getElementById('back').innerHTML, {log: true, ignoreAnimation: true});
    backcan.click();
    document.getElementById('savebuttons').setAttribute('style', '');
  });
  document.getElementById('savefront').addEventListener( 'click', function (e) {
    this.href = document.getElementById('frontimage').toDataURL().replace("image/png", "image/octet-stream;content-disposition:attachment; filename=front.png");
  });
  document.getElementById('saveback').addEventListener( 'click', function (e) {
    this.href = document.getElementById('backimage').toDataURL().replace("image/png", "image/octet-stream;content-disposition:attachment; filename=back.png");
  });
});
