# Ingress Stuff

### Stuff I made for Ingress

---

## Badge Builder

#### Badge Builder is a tool for creating custom identification badges for Ingress Agents.

This easy to use tool will allow you to have a completely custom and community specific badge for Ingress Field Agents.  Place the appropriate information in each field, add you community emblem, and you'll have a completely custom badge showing support and pride for you Ingress faction and community.

### [Demo](http://bt.tl/ingress/badgebuilder.html)

### Help Out

Badge Builder was made to be highly configurable.  If you have different designs for any aspect of this (background design, images, etc.), it can be incorporated.

### Additional Details

- Primary font is Coda from [Google Web Fonts](https://www.google.com/fonts/specimen/Coda)
- Agent Level Font is [UA Cadet Regular](http://boards.sportslogos.net/topic/93431-fonts-by-conrad-website-misc-pack-1-now-up/page-16#entry2125729)
- Barcode Font is [Code 128](http://www.barcodelink.net/barcode-font.php)
- Ingress symbols are from [Cory Hughart's Ingress Logos](http://cr0ybot.github.io/ingress-logos/)

---

I'll add more as I make things...

---

Copyright (c) 2014 Trae Blain

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

